#language:pt

Funcionalidade: Validar assinatura eletronica OPB
@auth
Cenario: realizar validação de assinatura eletronica open banking
Dado que eu possua o cpf "55983117475"
E que eu possua a senha "300621"
E que eu possua a assinatura eletronica <assinatura eletronica>
Quando eu realizar a requisicao
Entao eu devo receber o <resultado>

Exemplos:
|assinatura eletronica   |resultado     |
|"010721"                |"1"           |
|"210721"                |"assinatura informada não é valida. Favor verificar."|



