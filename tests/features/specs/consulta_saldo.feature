#language:pt

Funcionalidade: Consultar saldo da conta
@saldo
Cenario: realizar login no ib
Dado que eu possua o cpf "10607336927"
E que eu possua a senha "900800"
Quando eu realizar a requisicao POST no endpoint "http://dev-omni-ib-openbanking.omniaws.local/api/bank-balance"
Entao eu espero receber o status "200"
E eu espero receber o campo "referenceDate" com "integer"
E eu espero receber o campo "partialBalance" com "integer"
E eu espero receber o campo "withdrawBalance" com "integer"
E eu espero receber o campo "availableLimit" com "integer"
E eu espero receber o campo "totalBalance" com "integer"
