#language:pt
@consulta_pgto
Funcionalidade: Consultar iniciação de pagamento

Cenario: Dados do pagamento obtidos com sucesso.
Dado que eu possua o header "header consulta pagamento"
E eu possua o paymentId "TXpRMU9UQTROMWhZV2xSU1FUazJSMDl"
Quando eu realizar a requisição GET no endpoint "/v1/pix/payments/"
Entao Eu espero que o status da requisição seja "200"

Cenario: PaymentId invalido.
Dado que eu possua o header "header consulta pagamento"
E eu possua o paymentId "PaymentIDinvalido"
Quando eu realizar a requisição GET no endpoint "/v1/pix/payments/"
Entao Eu espero que o status da requisição seja "200"


@idempotencia
Cenario: Idempotencia.
Dado que eu possua o header "header consulta pagamento"
E eu possua o paymentId "TXpRMU9UQTROMWhZV2xSU1FUazJSMDl"
Quando eu realizar a requisição GET no endpoint "/v1/pix/payments/"
#resultado idempotencia
E eu realizar a requisição GET no endpoint "/v1/pix/payments/"
Entao Eu espero que o status da requisição seja "200" 



#Idempotência

# @2
# Cenario: A requisição foi malformada, omitindo atributos obrigatórios, seja no payload ou através de atributos na URL.
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "400"
# @3
# Cenario: Cabeçalho de autenticação ausente/inválido ou token inválido
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "401"
# @4
# Cenario: O token tem escopo incorreto ou uma política de segurança foi violada
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "403"
# @5
# Cenario: O recurso solicitado não existe ou não foi implementado
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "404"
# @6
# Cenario: O consumidor tentou acessar o recurso com um método não suportado
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "405"
# @7
# Cenario: A solicitação continha um cabeçalho Accept diferente dos tipos de mídia permitidos ou um conjunto de caracteres diferente de UTF-8
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "406"
# @8
# Cenario: O formato do payload não é um formato suportado.
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "415"

# @9
# Cenario: A operação foi recusada, pois muitas solicitações foram feitas dentro de um determinado período ou o limite global de requisições concorrentes foi atingido
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "429"
# @10
# Cenario: Ocorreu um erro no gateway da API ou no microsserviço	
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "500"



