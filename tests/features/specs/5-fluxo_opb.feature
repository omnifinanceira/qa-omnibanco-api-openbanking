#language:pt
@fluxo
Funcionalidade: Realizar pagamento via open banking

@fluxo_1
Cenario: Realizar pagamento com sucesso PF
Dado que eu receba uma solicitação de consentimento <cpf_debtor> <cpf_creditor> <nome_creditor> <valor_pix> <cc_creditor> <cc_debtor>
E que eu possua o cpf <cpf_debtor>
E que eu possua a senha <senha_debtor>
E que eu possua a assinatura eletronica <ass_debtor>
Quando eu me autenticar no openbanking com sucesso
E eu selecionar a conta
E eu visualizar os dados da compra
E eu autorizar o consentimento
E eu autorizar o pagamento
Entao Eu espero visualizar a tela de pedido concluido

Exemplos:
|cpf_debtor   |cpf_creditor |nome_creditor    |valor_pix|cc_creditor|cc_debtor|senha_debtor|ass_debtor|
|"44463484260"|"49462858659"|"OPEN BANKING UM"|"199.30" |"755307"   |"755404" |"004004"    |"400400"  |

@fluxo_2
Cenario: Conta sem saldo PF
Dado que eu receba uma solicitação de consentimento
Quando eu me autenticar no openbanking com sucesso
E eu selecionar a conta sem saldo
E eu visualizar os dados da compra
E eu autorizar o pagamento
Entao Eu espero visualizar mensagem de saldo insuficiente

@fluxo_3
Cenario: Assinatura eletronica incorreta
Dado que eu receba uma solicitação de consentimento
Dado que eu possua o cpf "44463484260"
E que eu possua a senha "001001"
E que eu possua a assinatura eletronica "123123"
Quando eu me autenticar no openbanking com sucesso
E eu selecionar a conta
E eu visualizar os dados da compra
E eu autorizar o pagamento
Entao Eu espero visualizar mensagem de assinatura eletronica incorreta

Cenario: Token incorreto
Dado que eu receba uma solicitação de consentimento
Quando eu me autenticar no openbanking com sucesso
E eu selecionar a conta
E eu visualizar os dados da compra
E eu autorizar o pagamento
E eu preencher o token incorreto
Entao Eu espero visualizar mensagem de token incorreto

Cenario: Token expirado
Dado que eu receba uma solicitação de consentimento
Quando eu me autenticar no openbanking com sucesso
E eu selecionar a conta
E eu visualizar os dados da compra
E eu autorizar o pagamento
E eu preencher o token expirado
Entao Eu espero visualizar mensagem de token incorreto

Cenario: Mudar conta
Dado que eu receba uma solicitação de consentimento
Quando eu me autenticar no openbanking com sucesso
E eu selecionar a conta
E eu visualizar os dados da compra
E selecionar opcoes da conta
E eu selecionar outra conta
E eu visualizar os dados da compra
E eu autorizar o pagamento
Entao Eu espero visualizar a tela de pedido concluido

Cenario: Cancelar operação
Dado que eu receba uma solicitação de consentimento
Quando eu me autenticar no openbanking com sucesso
E eu selecionar a conta
E eu visualizar os dados da compra
E eu cancelar o pagamento
Entao Eu espero visualizar mensagem de cancelamento

Cenario: 5 minutos 
Dado que eu receba uma solicitação de consentimento
Quando eu me autenticar no openbanking com sucesso
E eu selecionar a conta
E eu visualizar os dados da compra
E eu aguardo 5 minutos
Então eu espero visualizar mensagem que o consentimento expirou

Cenario: Realizar pagamento com sucesso PJ
Dado que eu receba uma solicitação de consentimento
Quando eu me autenticar no openbanking com sucesso
E eu selecionar a conta PJ
E eu visualizar os dados da compra
E eu autorizar o pagamento
Entao Eu espero visualizar a tela de pedido concluido


@multiplos_aut
Cenario: Realizar pagamento com sucesso - Multiplas autorizacoes
Dado que eu receba uma solicitação de consentimento
E que eu possua o cpf "49462858659"
E que eu possua a senha "003003"
E que eu possua a assinatura eletronica "300300"
Quando eu me autenticar no openbanking com sucesso
E eu selecionar a conta Iniciadora
E eu visualizar os dados da compra
E eu autorizar o pagamento
Entao Eu espero visualizar mensagem de aviso sobre multipla autorização

# Cenario: Realizar pagamento com sucesso - Multiplas autorizacoes - full - aceitar
# Dado que eu receba uma solicitação de consentimento
# Quando eu me autenticar no openbanking com sucesso
# E eu selecionar a conta Iniciadora
# E eu visualizar os dados da compra
# E eu autorizar o pagamento
# E eu realizar o fluxo de autorização - aceitar
# Entao Eu espero visualizar a tela de pedido concluido

# Cenario: Realizar pagamento com sucesso - Multiplas autorizacoes - full - aceitar - maior que dois aprovadores
# Dado que eu receba uma solicitação de consentimento
# Quando eu me autenticar no openbanking com sucesso
# E eu selecionar a conta Iniciadora
# E eu visualizar os dados da compra
# E eu autorizar o pagamento
# E eu realizar o fluxo de autorização - aceitar
# Entao Eu espero visualizar a tela de pedido concluido

# Cenario: Realizar pagamento com sucesso - Multiplas autorizacoes - full - Recusar
# Dado que eu receba uma solicitação de consentimento
# Quando eu me autenticar no openbanking com sucesso
# E eu selecionar a conta Iniciadora
# E eu visualizar os dados da compra
# E eu autorizar o pagamento
# E eu realizar o fluxo de autorização - recusar
# Entao Eu espero visualizar a tela de pedido concluido

# Cenario: Realizar pagamento com sucesso - Multiplas autorizacoes - full - Expirar
# Dado que eu receba uma solicitação de consentimento
# Quando eu me autenticar no openbanking com sucesso
# E eu selecionar a conta Iniciadora
# E eu visualizar os dados da compra
# E eu autorizar o pagamento
# E eu realizar o fluxo de autorização
# Entao Eu espero visualizar a tela de pedido concluido




