#language:pt
@fluxo
Funcionalidade: Realizar pagamento via open banking

@hp_1
Cenario: Realizar pagamento com sucesso PF
Dado que eu receba uma solicitação de consentimento
Dado que eu possua o cpf "49462858659"
E que eu possua a senha "003003"
E que eu possua a assinatura eletronica "300300"
#http://dev-omni-ib-auth.omniaws.local/oauth/token
Quando eu me autenticar no openbanking com sucesso
E eu selecionar a conta
#/api/payments/information/{consentId}
E eu visualizar os dados da compra
#/api/consents/{consentId}/validate
E eu autorizar o pagamento
#**PATCH /payments/consents///{consentId}///authorisations**\n+additional Infos
Entao Eu espero visualizar a tela de pedido concluido

@hp_2
Cenario: Realizar pagamento com sucesso PF
Dado que eu receba uma solicitação de consentimento
Dado que eu possua o cpf "44463484260"
E que eu possua a senha "001001"
E que eu possua a assinatura eletronica "123123"
#http://dev-omni-ib-auth.omniaws.local/oauth/token
Quando eu me autenticar no openbanking com sucesso
E eu selecionar a conta
#/api/payments/information/{consentId}
E eu visualizar os dados da compra
#/api/consents/{consentId}/reject
E eu cancelar o pagamento
Entao Eu espero visualizar consentId status rejeitado