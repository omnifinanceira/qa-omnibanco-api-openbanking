#language:pt
@criar_pagto
Funcionalidade: Criar uma iniciação de pagamento

@sucesso
Cenario: Criar iniciação de pagamento com sucesso
Dado que eu possua o header "header criar pagamento"
E eu possua o body "body criar pagamento"
Quando eu realizar um post no endpoint "/v1/pix/payments"
Entao Eu espero que o status da requisição seja "201"


@data @sucesso
Cenario: Validar dados do objeto 'data'
Dado que eu possua o header "header criar pagamento"
E eu possua o body "body criar pagamento"
Quando eu realizar um post no endpoint "/v1/pix/payments"
Entao Eu espero que o status da requisição seja "201"
E eu espero que o response possua o campo "paymentId"
E eu espero que o response possua o campo "endToEndId"
E eu espero que o response possua o campo "consentId"
E eu espero que o response possua o campo "creationDateTime"
E eu espero que o response possua o campo "statusUpdateDateTime"
E eu espero que o response possua o campo "proxy"
E eu espero que o response possua o campo "status"
E eu espero que o response possua o campo "rejectionReason"
E eu espero que o response possua o campo "localInstrument"
E eu espero que o response possua o campo "payment"
E eu espero que o response possua o campo "remittanceInformation"
E eu espero que o response possua o campo "creditorAccount"

@payment @sucesso
Cenario: Validar dados do objeto [data][payments]
Dado que eu possua o header "header criar pagamento"
E eu possua o body "body criar pagamento"
Quando eu realizar um post no endpoint "/v1/pix/payments"
Entao Eu espero que o status da requisição seja "201"
E eu espero que o response possua o campo "paymentId"
E eu espero que o response possua o campo "payment"
E eu espero que dentro do objeto "payment" eu possua o campo "amount"
E eu espero que dentro do objeto "payment" eu possua o campo "currency"

@creditorAccount @sucesso
Cenario: Validar dados do objeto [data][creditorAccount]
Dado que eu possua o header "header criar pagamento"
E eu possua o body "body criar pagamento"
Quando eu realizar um post no endpoint "/v1/pix/payments"
Entao Eu espero que o status da requisição seja "201"
E eu espero que o response possua o campo "paymentId"
E eu espero que o response possua o campo "payment"
E eu espero que dentro do objeto "creditorAccount" eu possua o campo "ispb"
E eu espero que dentro do objeto "creditorAccount" eu possua o campo "issuer"
E eu espero que dentro do objeto "creditorAccount" eu possua o campo "number"
E eu espero que dentro do objeto "creditorAccount" eu possua o campo "accountType"

@sucesso_autorizado
Cenario: status do consentimento CONSUMED
Dado que eu possua o header "header criar pagamento"
E eu possua o body autorizado
#**PATCH /payments/consents/{consentId}/consummations **\n+ additional Infos
Quando eu realizar um post no endpoint "/api/payments"
Entao Eu espero que o status da requisição seja "204"

@consentimento_utilizado
Cenario: Criar iniciação de pagamento com sucesso
Dado que eu possua o header "header criar pagamento"
E eu possua o body "body criar pagamento"
E eu possuir um consent já utilizado
Quando eu realizar um post no endpoint "/v1/pix/payments"
Entao Eu espero que o status da requisição seja "201"






# @2
# Cenario: A requisição foi malformada, omitindo atributos obrigatórios, seja no payload ou através de atributos na URL.
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "400"
# @3
# Cenario: Cabeçalho de autenticação ausente/inválido ou token inválido
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "401"
# @4
# Cenario: O token tem escopo incorreto ou uma política de segurança foi violada
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "403"
# @5
# Cenario: O recurso solicitado não existe ou não foi implementado
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "404"
# @6
# Cenario: O consumidor tentou acessar o recurso com um método não suportado
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "405"
# @7
# Cenario: A solicitação continha um cabeçalho Accept diferente dos tipos de mídia permitidos ou um conjunto de caracteres diferente de UTF-8
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "406"
# @8
# Cenario: O formato do payload não é um formato suportado.
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "415"
# @9
# Cenario: A solicitação foi bem formada, mas não pôde ser processada devido à lógica de negócios específica da solicitação.
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "422"
# @10
# Cenario: A operação foi recusada, pois muitas solicitações foram feitas dentro de um determinado período ou o limite global de requisições concorrentes foi atingido
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "429"
# @11
# Cenario: Ocorreu um erro no gateway da API ou no microsserviço	
# Dado que eu possua o "Header"
# E eu possua o "Body"
# Quando eu realizar a requisição 
# Entao Eu espero que o status da requisição seja "500"



