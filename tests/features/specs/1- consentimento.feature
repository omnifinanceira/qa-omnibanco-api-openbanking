#language:pt
@criar_consent
Funcionalidade: Criar um consentimento - Iniciadora

@consent
Cenario: Criar consentimento MANU
Dado que eu possua os dados do consentimento <cpf_debtor> <cpf_creditor> <nome_creditor> <valor_pix> <cc_creditor> <cc_debtor>
E que eu possua o localInstrument "MANU"
Quando eu realizar um post no endpoint "/open-banking/journey-tpp/v1/payments/consents"
Entao Eu espero que o status da requisição seja "201"

Cenario: Criar consentimento INIC
Dado que eu possua os dados do consentimento <cpf_debtor> <cpf_creditor> <nome_creditor> <valor_pix> <cc_creditor> <cc_debtor> <proxy-key>
E que eu possua o localInstrument "INIC"
Quando eu realizar um post no endpoint "/open-banking/journey-tpp/v1/payments/consents"
Entao Eu espero que o status da requisição seja "201"

Cenario: Criar consentimento DICT
Dado que eu possua os dados do consentimento <cpf_debtor> <cpf_creditor> <nome_creditor> <valor_pix> <cc_creditor> <cc_debtor> <proxy-key>
E que eu possua o localInstrument "DICT"
Quando eu realizar um post no endpoint "/open-banking/journey-tpp/v1/payments/consents"
Entao Eu espero que o status da requisição seja "201"



@data
Cenario: Validar dados do objeto 'data'
Quando eu realizar um post no endpoint "/v1/consents"
Entao Eu espero que o status da requisição seja "201"
E eu espero que o response possua o campo "paymentId"
E eu espero que o response possua o campo "endToEndId"
E eu espero que o response possua o campo "consentId"
E eu espero que o response possua o campo "creationDateTime"
E eu espero que o response possua o campo "statusUpdateDateTime"
E eu espero que o response possua o campo "proxy"
E eu espero que o response possua o campo "status"
E eu espero que o response possua o campo "rejectionReason"
E eu espero que o response possua o campo "localInstrument"
E eu espero que o response possua o campo "payment"
E eu espero que o response possua o campo "remittanceInformation"
E eu espero que o response possua o campo "creditorAccount"

@payment
Cenario: Validar dados do objeto [data][payments]
Quando eu realizar um post no endpoint "/api/payments"
Entao Eu espero que o status da requisição seja "201"
E eu espero que o response possua o campo "paymentId"
E eu espero que o response possua o campo "payment"
E eu espero que dentro do objeto "payment" eu possua o campo "amount"
E eu espero que dentro do objeto "payment" eu possua o campo "currency"

@creditorAccount
Cenario: Validar dados do objeto [data][creditorAccount]
Quando eu realizar um post no endpoint "/api/payments"
Entao Eu espero que o status da requisição seja "201"
E eu espero que o response possua o campo "paymentId"
E eu espero que o response possua o campo "payment"
E eu espero que dentro do objeto "creditorAccount" eu possua o campo "ispb"
E eu espero que dentro do objeto "creditorAccount" eu possua o campo "issuer"
E eu espero que dentro do objeto "creditorAccount" eu possua o campo "number"
E eu espero que dentro do objeto "creditorAccount" eu possua o campo "accountType"
