#language:pt
@consulta_consent
Funcionalidade: Consultar COnsentimento 
#Receber o consentimento da sensedia.
Cenario: Dados do consentimento obtidos com sucesso.
Dado que eu possua o header "header consulta consentimento"
E eu possua o ConsentId "urn:enq-v096lh968:KoIL"
Quando eu realizar a requisição GET no endpoint "/v1/consents/"
Entao Eu espero que o status da requisição seja "200"

Cenario: Dados do consentimento obtidos com sucesso. - Lydians
Dado que eu possua o header "header consulta consentimento"
E eu possua o ConsentId "urn:enq-v096lh968:KoIL"
Quando eu realizar a requisição GET no endpoint "/v1/consents/"
Entao Eu espero que o status da requisição seja "200"

Cenario: Dados do consentimento após 5 minutos
Dado que eu possua o header "header consulta consentimento"
E eu possua o ConsentId "urn:enq-v096lh968:KoIL"
Quando eu realizar a requisição GET no endpoint "/v1/consents/"
Entao Eu espero que o status da requisição seja "200"
E eu espero que o response possua o campo "status" com "REJECTED"

#**PATCH /payments/consents///{consentId}///authorisations**\n+additional Infos??

#POST /payments/v1/consents  \nBody{data+initiator+approvedconsent}\n status=AUTHORISED\n http://sandbox.lydians.com.br:2580/docs/index.html