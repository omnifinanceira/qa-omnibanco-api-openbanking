require_relative "../pages/request.rb"
require_relative "../pages/utils.rb"
require "httparty"
require "cpf_cnpj"
require "faker"
require "json-schema"
require "jsonpath"
require "pry"
require "yaml"
require "securerandom"

HEADERS = YAML.load_file("mock/header.yml")
BODY = YAML.load_file("mock/body.yml")
#ENV["HTTP_PROXY"] = "http://victor.debaza@proxy.omni.com.br:8080"
#ENV["HTTPS_PROXY"] = "https://victor.debaza:Julho2021@proxy.omni.com.br:8080"
