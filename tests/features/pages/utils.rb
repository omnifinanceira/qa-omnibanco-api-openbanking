require "uri"
require "net/http"

class Utils < Request
  attr_accessor :url

  def initialize
    @get_conta = "https://dev-ib-api.omni.com.br/api/v1/v2/usuarios/"
    @oauth = "https://dev-openfinance.omni.com.br/auth/oauth/token"
    @authentication = "http://dev-omni-ib-openbanking.omniaws.local/api/authentication"
  end

  def info_conta(cpf)
    headers = { 'Content-Type': "application/x-www-form-urlencoded" }
    response = HTTParty.get(@get_conta + cpf,
                            :headers => headers)
  end

  def post_basic_auth(cpf, senha) # Primeira request de autenticação // pega o Refresh_token
    headers = { 'Content-Type': "application/x-www-form-urlencoded" }
    body = { 'grant_type': "password",
             'username': "#{cpf}",
             'password': "#{senha}" }
    #binding.pry

    request = exec_post_aut_ws(@oauth, headers, body)
  end

  def post_basic_auth_full(cpf, senha) #Autentica e retorna o Access_token
    request = post_basic_auth(cpf, senha)
    refresh_token = request["refresh_token"]
    numero_agencia = request["userData"]["accounts"][0]["agencyNumber"]
    numero_conta = request["userData"]["accounts"][0]["accountNumber"]

    headers = { 'Content-Type': "application/x-www-form-urlencoded" }
    body = { 'grant_type': "account_select",
             'refresh_token': "#{refresh_token}",
             'numero_agencia': "#{numero_agencia}",
             'numero_conta': "#{numero_conta}" }
    result = exec_post_aut_ws(@oauth, headers, body)
    access_token = result["access_token"]
  end

  def selecionar_conta(refresh_token, numero_conta)
    headers = { 'Content-Type': "application/x-www-form-urlencoded" }
    body = { 'grant_type': "account_select",
             'refresh_token': "#{refresh_token}",
             'numero_agencia': "0001",
             'numero_conta': "#{numero_conta}" }
    result = exec_post_aut_ws(@oauth, headers, body)
    access_token = result["access_token"]
  end

  def authentication(cpf, senha, assinatura) #validar assinatura eletronica
    access_token = post_basic_auth_full(cpf, senha)

    headers = { 'Assinatura-Eletronica': "#{assinatura}",
                'access_token': "#{access_token}" }
    request = exec_post_aut_ws(@authentication, headers, body)
  end
end
