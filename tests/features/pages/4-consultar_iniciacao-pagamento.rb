class Consultar_pagamento < Request
  attr_accessor :url, :headers, :body

  def initialize
    @url = "https://sandbox-apigw.omni.com.br/open-banking/payments"
    @headers = {
      #'Accept': "*/*",
      'Content-Type': "application/json;charset=UTF-8",
    # 'x-fapi-auth-date': "Sun, 10 Sep 2017 19:43:31 UTC",
    # 'x-fapi-customer-ip-address': "127.0.0.1",
    # 'x-fapi-interaction-id': "SBvzxlf0-FhbmKX6jDFzEBBWPM6yvEaHivc7tuqE0e83Gy0uriJTq3R7xSiyFq-V8vOdtRB475bd7E0L25M8s7cHsKp6UUj",
    # 'x-customer-user-agent': "PostmanRuntime/7.28.0",

    }
  end

  def consultar_iniciacao_pagamento(endpoint, param, header)
    request = exec_get(@url + endpoint + param, header)
  end
end
