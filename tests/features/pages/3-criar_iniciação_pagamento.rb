class Iniciar_pagamento < Request
  attr_accessor :url, :headers, :body

  def initialize
    @url = "http://10.150.19.78:5000/payments/v1/pix/payments/"
  end

  def realizar_iniciacao_pagamento(endpoint, header, body)
    request = exec_post_aut(url + endpoint, header, body)
  end
end
