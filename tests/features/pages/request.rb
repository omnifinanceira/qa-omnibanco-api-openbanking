class Request
  def initialize
    #@url = "https://dev-omni-ib-openbanking.omniaws.local/api"
    @url = "https://sandbox-apigw.omni.com.br/open-banking/payments"
    #@base_uri = "http://localhost:8082"
    #@base_uri_simular = "https://dev-omnimais.omni.com.br/api"
    @body = nil
  end

  def exec_post_aut(path, headers, body)
    url_full = @url + path
    response = HTTParty.post(url_full,
                             :body => body.to_json,
                             :headers => headers)

    return response
  end

  def exec_post_aut_ws(path, headers, body = @body) #USADO APENAS PRA AUTENTICAÇÂO NAO MEXER
    url_full = path
    auth = { :username => "angular", :password => "secret" }
    if body == nil
      @response = HTTParty.post(url_full,
                                :headers => headers,
                                :basic_auth => auth)
    else
      @response = HTTParty.post(url_full,
                                :body => body,
                                :headers => headers,
                                :basic_auth => auth)
    end
    return @response
  end

  def exec_get(path, param, headers)
    url_full = @url + path
    response = HTTParty.get(url_full + param,
                            :headers => headers)
    return response
  end
end
