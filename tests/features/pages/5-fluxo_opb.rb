class Pagamentos < Request
  attr_reader :url_full, :consentId, :creationDateTime, :expirationDateTime, :headers, :xIdempotencyKey

  def initialize
    @url_full = "http://10.150.19.78:5000/payments/v1/consents"
    @xIdempotencyKey = SecureRandom.uuid
    @creationDateTime = Time.now.strftime("%Y-%m-%d")
    @expirationDateTime = (Time.now + 300).strftime("%Y-%m-%dT%H:%M:%S.549Z")
  end

  def gerar_consentimento_sensedia(cpf_debtor, cpf_creditor, nome_creditor, valor_pix, cc_creditor, cc_debtor, localInstrument = "MANU")
    at = gerar_oauth_sensedia
    url_full = "https://sandbox-apigw.omni.com.br/open-banking/journey-tpp/v1/payments/consents"
    header = { 'X-Idempotency-Key': "#{@xIdempotencyKey}",
               "Authorization": "Bearer #{at["access_token"]}",
               'Content-Type': "application/json" }
    body = {
      "authorisationServer": {
        "authorisationServerId": "52769c42-9463-4b1d-b354-7ac74ddbd07a",
        "organisationId": "0dd36270-d5d6-5bec-86ca-c9b8fcb015dd",
      },
      "businessEntity": {
        "document": {
          "identification": "82782693000186",
          "rel": "CNPJ",
        },
      },
      "loggedUser": {
        "document": {
          "identification": "#{cpf_debtor}",
          "rel": "CPF",
        },
      },
      "creditor": {
        "personType": "PESSOA_NATURAL",
        "cpfCnpj": "#{cpf_creditor}",
        "name": "#{nome_creditor}",
      },
      "payment": {
        "type": "PIX",
        "date": "#{@creationDateTime}",
        "currency": "BRL",
        "amount": "#{valor_pix}",
        "ibgeTownCode": "5300108",
        "details": {
          "localInstrument": "#{localInstrument}",
          "proxy": "44463484260",
          "creditorAccount": {
            "ispb": "60850229",
            "issuer": "1",
            "number": "#{cc_creditor}",
            "accountType": "CACC",
          },
        },
      },
      "debtorAccount": {
        "ispb": "60850229",
        "issuer": "0001",
        "number": "#{cc_debtor}",
        "accountType": "CACC",
      },
      "remittanceInformation": "Informação adicional",
      "cnpjInitiator": "99999001000141",
    }
    begin
      response = HTTParty.post(url_full, :body => body.to_json, :headers => header)
    rescue Exception => ex
      puts(ex.message)
    end
  end

  def aprovar_consentimento(access_token, assinatura_eletronica, consentId, omni_token)
    url = "https://dev-ib-api.omni.com.br/openbanking/api/consents/#{consentId}/validate"
    headers = {
      'authorization': "#{access_token}",
      'assinatura_eletronica': "#{assinatura_eletronica}",
      'omni_token': "#{omni_token}",
    }
    begin
      response = HTTParty.post(url, :headers => headers)
    rescue Exception => ex
      puts(ex.message)
    end
  end

  def gerar_consentimento_autorizado(cpf_user, consentId)
    headers = {
      'Content-Type': "application/json;charset=UTF-8",
      'Authorization': "646546546",
      'X-Idempotency-Key': "#{@xIdempotencyKey}",
    }
    body = {
      "data": {
        "loggedUser": {
          "document": {
            "identification": "35307333854",
            "rel": "CPF",
          },
        },
        "creditor": {
          "personType": "PESSOA_NATURAL",
          "cpfCnpj": "49462858659",
          "name": "OPEN BANKING UM",
        },
        "businessEntity": {
          "document": {
            "identification": "91196809000196",
            "rel": "CNPJ",
          },
        },
        "debtorAccount": {
          "ispb": "60850229",
          "issuer": "0001",
          "number": " 755404",
          "accountType": "CACC",
        },
        "payment": {
          "type": "PIX",
          "date": "#{Time.now.strftime("%Y-%m-%d")}",
          "currency": "BRL",
          "amount": "290.12",
        },
      },
      "approvedConsent": {
        "consentId": "#{consentId}",
        "status": "AUTHORISED",
        "creationDateTime": "#{@creationDateTime}",
        "expirationDateTime": "#{@expirationDateTime}",
      },
    }

    begin
      response = HTTParty.post(@url_full, :body => body.to_json, :headers => headers)
    rescue Exception => ex
      puts(ex.message)
    end
  end

  def consultar_consentimento_lydians(link)
    responseget = HTTParty.get("http://10.150.19.78:5000/payments/v1/" + link, :headers => headers)

    puts responseget
  end

  def post_pagamento(consentID, access_token, valor, cc_creditor, localInstrument = "MANU")
    url = "https://dev-omni-ib-openbanking.dev-omni-ib.sa-east-1.omniaws.io/opbgw/api/payments"
    headers = {
      'Content-Type': "application/json;charset=UTF-8",
      'Authorization': "#{access_token}",
      'X-Idempotency-Key': "#{@xIdempotencyKey}",
    }
    body = {
      "data": {
        "localInstrument": "#{localInstrument}",
        "payment": {
          "amount": "#{valor}",
          "currency": "BRL",
        },
        "creditorAccount": {
          "ispb": "60850229",
          "issuer": "0001",
          "number": "#{cc_creditor}",
          "accountType": "CACC",
        },
        "remittanceInformation": "Pagamento da nota XPTO035-002.",
      #"proxy": "12345678901",
      #"cnpjInitiator": "50685362000135",
      #"transactionIdentification": "E00038166201907261559y6j6",
      #"ibgeTownCode": "5300108"
      #"qrCode": "00020104141234567890123426660014BR.GOV.BCB.PIX014466756C616E6F32303139406578616D706C652E636F6D27300012  \nBR.COM.OUTRO011001234567895204000053039865406123.455802BR5915NOMEDORECEBEDOR6008BRASILIA61087007490062  \n530515RP12345678-201950300017BR.GOV.BCB.BRCODE01051.0.080450014BR.GOV.BCB.PIX0123PADRAO.URL.PIX/0123AB  \nCD81390012BR.COM.OUTRO01190123.ABCD.3456.WXYZ6304EB76\n",
      },
      "consentId": "#{consentID}",
      "cnpjInitiator": "99999001000141",
    }
    begin
      response = HTTParty.post(url, :body => body.to_json, :headers => headers)
    rescue Exception => ex
      puts(ex.message)
    end
  end

  def consultar_dados(access_token, consentID)
    url = "https://dev-ib-api.omni.com.br/openbanking/api/payments/information/#{consentID}"
    headers = { 'Content-Type': "application/json;charset=UTF-8",
                'authorization': "#{access_token}" }
    responseget = HTTParty.get(url, :headers => headers)
  end

  def gerar_oauth_sensedia
    url_full = "https://sandbox-apigw.omni.com.br/oauth/v1/access-token"
    headers = {
      'Content-Type': "application/x-www-form-urlencoded",
      'Authorization': "Basic ZTEzYWI5ZWYtZjcwMy0zZWY5LTk2ZjktZDE0MjgzNDg2M2MwOmJmYTI2NDM5LTA5MjUtMzQxNy04MGRmLWQyMmY4YmYyZDJjMA==",
    }
    body = {
      'grant_type': "client_credentials",
    }
    begin
      response = HTTParty.post(url_full, :body => body, :headers => headers)
    rescue Exception => ex
      puts(ex.message)
    end
  end

  def pegar_consentid(url)
    uri = URI.parse(url)
    params = CGI.parse(uri.query)
    teste = params["scope"][0]
    teste.delete_prefix!("payments openid resources consent:")
  end
end
