Dado("eu possua o ConsentId {string}") do |string|
  @query_string = string
end

Quando("eu realizar a requisição GET no endpoint {string}") do |endpoint|
  consulta = Request.new
  @result = consulta.exec_get(endpoint, @query_string, @header)
end
