Entao("eu espero que o response possua o campo {string}") do |string|

  #@result["data"].each { |row| expect(!row[string].nil?).to be_truthy }

  expect(@result["data"][string]).to be_truthy
end

Entao("eu espero que dentro do objeto {string} eu possua o campo {string}") do |string, string2|
  expect(@result["data"][string][string2]).to be_truthy
end
