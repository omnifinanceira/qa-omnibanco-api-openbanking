Dado("que eu receba uma solicitação de consentimento {string} {string} {string} {string} {string} {string}") do |cpf_debtor, cpf_creditor, nome_creditor, valor_pix, cc_creditor, cc_debtor|
  @pag = Pagamentos.new
  @cpf_debtor = cpf_debtor
  @cc_debtor = cc_debtor
  @cc_creditor = cc_creditor
  @valor_pix = valor_pix
  consentimento = @pag.gerar_consentimento_sensedia(cpf_debtor, cpf_creditor, nome_creditor, valor_pix, cc_creditor, cc_debtor)
  @consentId = @pag.pegar_consentid(consentimento["redirect_uri"])
  puts @consentId
end

Quando("eu me autenticar no openbanking com sucesso") do
  @autent = Utils.new
  @login = @autent.post_basic_auth(@cpf_debtor, @senha)
end

Quando("eu selecionar a conta") do
  refresh_token = @login["refresh_token"]
  @acc_token_conta_selecionada = @autent.selecionar_conta(refresh_token, @cc_debtor)
  puts @acc_token_conta_selecionada
end

Quando("eu visualizar os dados da compra") do
  @dados_pgto = @pag.consultar_dados(@acc_token_conta_selecionada, @consentId)
  puts @dados_pgto
end
Quando("eu autorizar o consentimento") do
  binding.pry
  @response = @pag.aprovar_consentimento(@acc_token_conta_selecionada, @ass_eletr, @consentId, "348086") #string é o token
  puts @response
end

Quando("eu autorizar o pagamento") do
  @response_pag = @pag.post_pagamento(@consentId, @acc_token_conta_selecionada, @valor_pix, @cc_creditor)
  puts @response_pag
end

Entao("Eu espero visualizar a tela de pedido concluido") do
  binding.pry
end

Entao("Eu espero visualizar mensagem de assinatura eletronica incorreta") do
  binding.pry
end

Quando("eu selecionar a conta Iniciadora") do
  binding.pry
end

Entao("Eu espero visualizar mensagem de aviso sobre multipla autorização") do
  pending # Write code here that turns the phrase above into concrete actions
end
